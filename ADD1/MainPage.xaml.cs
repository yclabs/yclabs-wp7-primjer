﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace ADD1
{
    public partial class MainPage : PhoneApplicationPage
    {
        private MainPageViewModel mainPageViewModel;
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            mainPageViewModel = new MainPageViewModel();
            DataContext = mainPageViewModel;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            mainPageViewModel.Count++;
        }
    }
}